
> React is a JavaScript __*library*__ for building user interfaces.

---

##### Création d'un composant

```jsx
const userName = '@DecampsRenan';

export default class MyComponent extends React.Component {

    render() {
        return (
            <div>
                <h1>My Component</h1>
                <p>My name is {userName}</p>
            </div>
        );
    }
}
```

---

##### Création d'un composant

```jsx
const userName = '@DecampsRenan';

export const MyComponent = () => (
    <div>
        <h1>My Component</h1>
        <p>My name is {userName}</p>
    </div>
);
```

---

##### Props

```jsx
<MyComponent name="Ruban" />
```

```jsx
export const MyComponent = (props) => (
    <h1>{props.name}</h1>
);

// Ou encore plus simple et explicite
export const MyComponent = ({ name }) => (
    <h1>{name}</h1>
);
```

---

##### State

```jsx
export class MyComponent extends React.Component {

    state = {
        name: 'Say my name.',
    };

    componentDidMount() {
        // asynchronous code
        myServiceThatMakeAnHttpRequestAndGetMyName()
            .then(name => this.setState({ name }))
            .catch(error => console.warn('Oups...'));
    }

    render() {
        const { name } = this.state;
        return (
            <h1>{name}</h1>
        );
    }

}
```

---

##### Event handling

```jsx
export const MyComponent = ({ value, onChange }) => (
    <div>
        <p>{value}</p>
        <input
            type="text"
            onChange={onChange}
            value={value}
        />
    </div>
);
```

---

##### Event handling

```jsx
export class MyRootComponent extends React.Component {
    state = {
        value: 'Hey !',
    };

    handleOnChange = (event) => {
        this.setState({ value: event.target.value });
    }

    render() {
        const { value } = this.state;
        return (
            <MyComponent
                value={value}
                onChange={handleOnChange}
            />
        );
    }
}
```

---

<!-- .slide: data-background-image="/img/sad.gif" -->

##### Et mes requêtes Ajax ... ?

---

##### Au choix

- Axios
- Fetch
- Request
- Superagent
- And many more...

---

##### Et encore...

- Hooks
- Suspense
- Du routing - *React Router*
- Des animations - *react-transition-group*
- Une documentation ( reactjs.org )