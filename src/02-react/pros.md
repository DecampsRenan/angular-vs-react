#### Pros

- Très léger                                                       <!-- .element: class="fragment" -->
- Bibliothèque de composants de qualité conséquente                <!-- .element: class="fragment" -->
- Communauté très active                                    <!-- .element: class="fragment" -->
- Peut de breaking changes / et petits (cf. v16)                 <!-- .element: class="fragment" -->
- Excellent starter (create-react-app) <!-- .element: class="fragment" -->
- Stylisation plus simple <!-- .element: class="fragment" -->