<!--
    .slide: data-background="img/bearstudio-bg.png"
-->

# A propos

Renan Decamps  
Dev Front [@_BearStudio](https://twitter.com/_bearstudio)

[@DecampsRenan](https://twitter.com/decampsrenan)  

[![Logo Bearstudio](img/logotypejaune.png)](http://bearstudio.fr) <!-- .element: class="bs-logo" -->