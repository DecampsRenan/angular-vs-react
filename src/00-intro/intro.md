### Angular

- Créé par Google (2016)
- **Différent** d'AngularJS
- 47k stars sur Github

---

### React

- Créé par Facebook (2013)
- Répond à un besoin interne de perf
- 125k stars sur Github

Note:
- (55 000 de différence lors de la première conf, maintenant 78 000)