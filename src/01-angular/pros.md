#### Pros

- Tout est fourni                       <!-- .element: class="fragment" -->
- Une bonne documentation               <!-- .element: class="fragment" -->
- Syntaxe HTML friendly                 <!-- .element: class="fragment" -->
- Sympa quand on vient du monde backend <!-- .element: class="fragment" -->
- Courbe d'apprentissage correcte       <!-- .element: class="fragment" -->