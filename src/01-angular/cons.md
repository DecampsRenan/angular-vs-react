#### Cons

- Lourd lourd lourd...                                 <!-- .element: class="fragment" -->
- TypeScript un peu sévère sur la validation des types <!-- .element: class="fragment" -->
- Les bibliothèques de composants trop pauvres         <!-- .element: class="fragment" -->
- v2, v4, v5 ....: upgrades compliquées                <!-- .element: class="fragment" -->
- Le système d'injection de dépendances <!-- .element: class="fragment" -->
- Problèmes avec les sélecteurs (ex. boucler sur un td dans un tr) <!-- .element: class="fragment" -->