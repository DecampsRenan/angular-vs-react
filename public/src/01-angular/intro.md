> One __*framework*__.  
> Mobile & desktop.

---

**Framework**
> A [...] framework is a universal, reusable software environment that provides particular functionality as part of a larger software platform to facilitate development of software applications, products and solutions.

---

**Web Component**

```html
<video width="400" height="222" controls="controls">
  <source src="video/sintel.webm" type="video/webm" />
</video>
```

![Video HTML5](./img/video-html5.png)

---

##### Un moteur de templating

```typescript
import { MyService } from './my-service.service.ts';

@Component({
    selector: 'my-component',
    template: `
        <div>
            <h1>My Component</h1>
            <p>My name is {{ userName }}</p>
        </div>
    `,
})
export class MyComponent implements OnInit {

    userName: string;

    @Input() disabled = false;
    @Outpu() changeEvent = new EventEmitter();

    constructor(private myService: MyService) {}

    ngOnInit() {
        this.myService.getGithubAuthenticatedUser('yes-this-token-is-valid')
            .subscribe(githubAccount => this.userName = githubAccount.login);
    }

}
```

---

##### Directives

```html
<input
    type="text"
    *ngIf="isVisible"
    name="{{ model.name }}"
    [disabled]="isLoading"
    [(ngModel)]="model.value"
    (change)="handleOnChange($event)"
>
```

---

##### Client Http

```typescript
@Inject()
export class MyService {

    constructor(private http: HttpClient) {}

    getGithubAuthenticatedUser(token: string): Observable<any> {
        const params: URLSearchParams = new URLSearchParams();
        params.set('access_token', token);
        
        return this.http
            .get(`https://api.github.com/user`, { params });
            .map(data => data.json());
    }
}
```

---

##### Injection de dépendances

```typescript
import { MyComponent } from './my-component.component.ts';
import { MyService } from './my-service.service.ts';

@NgModule({
    declaration: [
        MyComponent,
    ],
    services: [
        MyService,
    ],
    exports: [
        MyComponent,
    ],
})
export class MyModule {}
```

---

##### Bootstraping

```typescript
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { MyModule } from './my-module.module.ts';

platformBrowserDynamic().bootstrapModule(MyModule);
```

```html
<body>
    <my-component></my-component>
</body>
```

---

##### Et encore...

- Des pipes / directives...
- Du routing
- Des animations
- Gestion des services workers
- Une documentation ( angular.io )