#### Cons

- Lourd lourd lourd...                                 <!-- .element: class="fragment" -->
- TypeScript un peu sévère sur la validation des types <!-- .element: class="fragment" -->
- Les bibliothèques de composants trop pauvres         <!-- .element: class="fragment" -->
- v2, v4, v5: upgrades compliquées                     <!-- .element: class="fragment" -->
- styles CSS ou utilisation de librairies              <!-- .element: class="fragment" -->