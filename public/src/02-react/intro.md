
> React is a JavaScript __*library*__ for building user interfaces.

---

##### Création d'un composant

```jsx
const userName = '@DecampsRenan';

export default class MyComponent extends React.Component {

    render() {
        return (
            <div>
                <h1>My Component</h1>
                <p>My name is {userName}</p>
            </div>
        );
    }
}
```

---

##### Création d'un composant

```jsx
const userName = '@DecampsRenan';

export const MyComponent = () => (
    <div>
        <h1>My Component</h1>
        <p>My name is {userName}</p>
    </div>
);
```

---

<!-- .slide: data-background-image="/img/sad.gif" -->

##### Et mes requêtes Ajax ... ?

---

##### Au choix

- Axios
- Fetch
- Request
- Superagent
- And many more...

---

##### Props

```jsx
<MyComponent name="Ruban"/>
```

```jsx
export const MyComponent = (props) => (
    <h1>{props.name}</h1>
);

// Ou plus simple
export const MyComponent = ({ name }) => (
    <h1>{name}</h1>
);
```

---

##### State

```jsx
export class MyComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: 'Say my name.',
        };
    }

    componentDidMount() {
        myServiceThatMakeAnHttpRequestAndGetMyName()
            .then((name) => this.setState({ name }))
            .catch((error) => console.warn('Oups...'));
    }

    render() {
        const { name } = this.state;
        return (
            <h1>{name}</h1>
        );
    }

}
```

---

##### Event handling

```jsx
export const MyComponent = ({ value, onChange }) => (
    <div>
        <p>{value}</p>
        <input
            change={onChange}
            value={value}
        />
    </div>
);

export class MyRootComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 'Hey !',
        };
    }

    handleOnChange = (event) => {
        console.log(event.target.value);
        this.setState({ value: event.target.value });
    }

    render() {
        const { value } = this.state;
        return (
            <MyComponent
                value={value}
                onChange={handleOnChange}
            />
        );
    }
}
```